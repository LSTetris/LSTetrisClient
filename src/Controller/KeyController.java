package Controller;

import Network.NetworkManager;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;

/**
 * La classe KeyController permet gestionar l'input de teclat dels clients del joc LSTetris.
 */
public class KeyController implements KeyListener{

    private NetworkManager networkManager;

    public KeyController(NetworkManager networkManager){
        this.networkManager = networkManager;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("KeyCode: " + e.getKeyCode() + " A: " + KeyEvent.VK_A);
        if(e.getKeyCode() == KeyEvent.VK_SPACE || (e.getKeyCode() >= KeyEvent.VK_A && e.getKeyCode() <= KeyEvent.VK_Z)
            || (e.getKeyCode() >= KeyEvent.VK_0 && e.getKeyCode() <= KeyEvent.VK_9)
                || e.getKeyCode() >= 37 && e.getKeyCode() <= 40) {

            System.out.println(e.getKeyText(e.getKeyCode()));
            networkManager.sendObject(new String[]{"Game", "Key"}, e.getKeyText(e.getKeyCode()));


        }


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
