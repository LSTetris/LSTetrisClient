package Controller;

import Network.NetworkManager;
import View.VistaMenu;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * La classe ViewWindowController permet gestionar els esdeveniments de finestra de la GUI.
 * Quan la finsetra principal es tanqui aturara la conexió amb el servidor.
 */
public class ViewWindowController implements WindowListener {

    private NetworkManager networkManager;
    private VistaMenu vistaMenu;

    /**
     * Crea un nou ViewWindowController
     * @param server es el gestor de xarxa del joc.
     */
    public ViewWindowController(NetworkManager server, VistaMenu vista){
        this.networkManager = server;
        this.vistaMenu = vista;
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

        //System.out.println(e.getWindow().getClass().toString());
        String[] pieces = e.getWindow().getClass().toString().split("\\.| ");
        String view = pieces[2];

        if (view.equals("VistaIniciSessio") || view.equals("VistaMenu") || view.equals("VistaRegistre")){
            e.getWindow().dispose();
            System.out.println("Closing client, telling connected server");
            networkManager.stopServerConnection();
        } else {
            e.getWindow().setVisible(false);
            vistaMenu.setVisible(true);
        }

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
