package Controller;

import Model.Match;
import Model.User;
import Model.UserManager;
import Model.RegisterExceptions.EmptyFieldException;
import Network.NetworkManager;
import View.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * La classe WindowController
 */
public class WindowController implements ActionListener{
    private final VistaMenu vistaMenu;
    private final VistaJoc vistaJoc;
    private final VistaIniciSessio vistaIniciSessio;
    private final VistaRegistre vistaRegistre;
    private final VistaConfiguracio vistaConfiguracio;
    private final VistaEspectador vistaEspectador;
    private final VistaPartidaAntiga vistaAntiga;

    private final UserManager manager;
    private final NetworkManager networkManager;


    public WindowController(VistaMenu vistaMenu,
                            VistaJoc vistaJoc,
                            VistaIniciSessio vistaIniciSessio,
                            VistaRegistre vistaRegistre,
                            VistaConfiguracio vistaConfiguracio,
                            VistaEspectador vistaEspectador,
                            VistaPartidaAntiga vistaAntiga,
                            UserManager manager,
                            NetworkManager network) {

        this.vistaMenu = vistaMenu;
        this.vistaJoc = vistaJoc;
        this.vistaIniciSessio = vistaIniciSessio;
        this.vistaRegistre = vistaRegistre;
        this.vistaConfiguracio = vistaConfiguracio;
        this.vistaEspectador = vistaEspectador;
        this.vistaAntiga = vistaAntiga;
        this.manager = manager;
        this.networkManager = network;
    }


    @Override
    public void actionPerformed (ActionEvent e) {
        switch (e.getActionCommand()) {
            case VistaMenu.JUGAR:
                vistaJoc.setVisible(true);
                vistaMenu.setVisible(false);
                networkManager.sendObject(new String[]{"Game", "Start"}, null);
                break;

            case VistaMenu.MODE_ESPECTADOR:
                vistaEspectador.setVisible(true);
                vistaMenu.setVisible(false);
                networkManager.sendObject(new String[]{"Spectator","Players"},null);
                break;

            case VistaMenu.REPRODUIR:
                vistaAntiga.setVisible(true);
                vistaMenu.setVisible(false);
                break;

            case VistaMenu.CONFIGURACIO:
                vistaConfiguracio.setVisible(true);
                vistaMenu.setVisible(false);
                break;

            case VistaMenu.SORTIR:
                System.out.println("Closing client, telling connected server");
                networkManager.stopServerConnection();
                System.exit(0);
                break;

            case VistaIniciSessio.INICIA_SESSIO:
                try {
                    //Obtenim els camps
                    String[] loginFields = vistaIniciSessio.getFields();
                    manager.validateLoginParameters(loginFields);

                    //Ho enviem al servidor
                    networkManager.sendObject(new String[]{"User", "Login"}, loginFields);
                } catch (EmptyFieldException e1) {
                    JOptionPane.showMessageDialog(vistaIniciSessio, e1.getMessage(), "Camp buit", JOptionPane.ERROR_MESSAGE);
                }

                break;

            case VistaIniciSessio.REGISTRARSE:
                vistaRegistre.setVisible(true);
                vistaIniciSessio.setVisible(false);
                break;

            case VistaRegistre.REGISTRAT:

                try {
                    //Obtenim els camps i els validem, gestionem les exceptions per mostrar una resposta adequada a l'usuari
                    String[] registerFields = vistaRegistre.getFields();
                    manager.validateRegisterParameters(registerFields);

                    //Ho enviem al servidor
                    networkManager.sendObject(new String[]{"User", "Register"}, new User(registerFields));

                } catch (Exception e1) {
                    System.out.println("Error en registrat");
                };

                break;

            case VistaRegistre.TORNA:
                vistaIniciSessio.setVisible(true);
                vistaRegistre.setVisible(false);
                break;

            case VistaConfiguracio.GUARDAR:
                if (teclesBuides()) {
                    JOptionPane.showMessageDialog(null, "Falta seleccionar alguna tecla", "Atenció", JOptionPane.WARNING_MESSAGE);
                } else {
                    if (teclesIguals()) {
                        JOptionPane.showMessageDialog(null, "Hi ha dos tecles iguals", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        int n = JOptionPane.showConfirmDialog(null, "Estàs segur que vols guardar?", "Atenció", JOptionPane.YES_NO_OPTION);
                        //Es vol guardar les tecles
                        if (n == 0) {
                            //TODO: Guardar les tecles al servidor

                            //Obtenim les tecles seleccionades
                            ArrayList<String> selectedKeys = vistaConfiguracio.getSelectedKeys();

                            networkManager.sendObject(new String[]{"KeyBinding"}, selectedKeys);

                            vistaMenu.setVisible(true);
                            vistaConfiguracio.setVisible(false);
                        }
                    }
                }

                break;

            case VistaConfiguracio.TORNAR:
                vistaMenu.setVisible(true);
                vistaConfiguracio.setVisible(false);
                break;

            case VistaEspectador.TORNA_MENU:
                vistaMenu.setVisible(true);
                vistaEspectador.setVisible(false);
                break;

            case VistaPartidaAntiga.TORNA_AL_MENU:
                vistaMenu.setVisible(true);
                vistaAntiga.setVisible(false);
                break;

            case VistaJoc.SORTIR_JOC:
                int n = JOptionPane.showConfirmDialog(null, "Vols sortir sense guardar?", "Atenció", JOptionPane.YES_NO_OPTION);
                //Es vol sortir sense guardar
                if (n == 0) {
                    networkManager.sendObject(new String[] {"Game", "Stop"}, null);
                    vistaMenu.setVisible(true);
                    vistaJoc.setVisible(false);
                }

                break;

            case VistaJoc.GUARDAR_JOC:
                missatgeFiPartida();
                break;

            case VistaEspectador.ENTRA:
                System.out.println("Entrant...");
                break;
        }
    }

    public void missatgeFiPartida() {
        int punts = 0;
        Object[] options = {"Guardar la partida", "Sortir sense guardar"};
        int n = JOptionPane.showOptionDialog(null,
                "Enhorabona, has obtingut "+ punts +" punts",
                "Fi de la partida",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[0]);
        if (n == 1) {
            //TODO: RESETEJAR TAULELL, PUNTUACIO, TEMPS...
            vistaMenu.setVisible(true);
            vistaJoc.setVisible(false);
        } else {
            //TODO: GUARDAR PARTIDA
        }
    }

    public void missatgeLoginIncorrecte() {
        JOptionPane.showMessageDialog(vistaIniciSessio, "L'usuari i/o la contrasenya són incorrectes", "Error", JOptionPane.ERROR_MESSAGE);
        vistaIniciSessio.setJtfNickname("");
        vistaIniciSessio.setJpfPassword("");
    }

    //Aquest mètode canvia a la vista de menu des del registre
    public void switchToMenuFromRegister(){
        vistaMenu.setVisible(true);
        vistaRegistre.setVisible(false);
    }
    //Canvia al menú des del login
    public void switchToMenuFromLogin(){
        vistaMenu.setVisible(true);
        vistaIniciSessio.setVisible(false);
    }

    private boolean teclesIguals () {

        ArrayList<String> tecles = vistaConfiguracio.getSelectedKeys();

        ArrayList<String> tempTecles = new ArrayList<>();

        for (String tecla : tecles) {
            if(!tempTecles.contains(tecla)){
                tempTecles.add(tecla);
            } else {
                return true;
            }
        }

        return false;
    }

    private boolean teclesBuides () {
        ArrayList<String> tecles = vistaConfiguracio.getSelectedKeys();
        return tecles.contains("Selecciona la lletra...");
    }

    public void registerGame(Match match){
        this.vistaJoc.registerMatch(match);
    }

    public void updateGameView() {
        this.vistaJoc.updateView();
    }

    public void actualitzaVistaEspectador(ArrayList<ArrayList<String>> games) {
        vistaEspectador.setMatches(games);
    }
}
