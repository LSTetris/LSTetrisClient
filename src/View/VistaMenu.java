package View;

import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

public class VistaMenu extends JFrame {
    //Actions commands accessibles des de qualsevol classe
    public static final String JUGAR = "Jugar";
    public static final String MODE_ESPECTADOR = "Mode Espectador";
    public static final String REPRODUIR = "Reproduir Partida Antiga";
    public static final String CONFIGURACIO = "Configuració";
    public static final String SORTIR = "Sortir";

    //Atributs que es necessiten per a implementar funcions
    private JButton jbJugar;
    private JButton jbEspectador;
    private JButton jbReproduir;
    private JButton jbConfiguracio;
    private JButton jbSortir;

    /**
     * */
    public VistaMenu() {
        //Propietats de la finestra
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        //Declaracio del Panell del Button de Jugar
        JPanel jpJugar = new JPanel();
        //Declaracio del Button de Jugar
        jbJugar = new JButton("Jugar");

        //Declaracio del Panell del Button de Mode Espectador
        JPanel jpEspectador = new JPanel();
        //Declaracio del Button del Mode Espectador
        jbEspectador = new JButton("Mode Espectador");

        //Declaracio del Panell del Button de Reproduir Partida Antiga
        JPanel jpReproduir = new JPanel();
        //Declaracio del Button de Reproduir Partida Antiga
        jbReproduir = new JButton("Reproduir Partida Antiga");

        //Declaracio del Panell del Button de Configuracio
        JPanel jpConfiguracio = new JPanel();
        //Declaracio del Button de Configuracio
        jbConfiguracio = new JButton("Configuració");

        //Declaracio del Panell del Button de Sortir
        JPanel jpSortir = new JPanel();
        //Declaracio del Button de Sortir
        jbSortir = new JButton("Sortir");

        //Declaracio del Panell Principal del joc
        JPanel jpMenuPrincipal = new JPanel();
        jpMenuPrincipal.setLayout(new GridBagLayout());
        Box content = Box.createVerticalBox();
        jpMenuPrincipal.add(content);

        //Declaracio de l'espai entre els Buttons del menu
        EmptyBorder espai = new EmptyBorder(13,0,15,0);

        //Declaracio del Panell de tots els Buttons
        JPanel jpButtons = new JPanel();
        jpButtons.setLayout(new BoxLayout(jpButtons, BoxLayout.Y_AXIS));

        //Es posa el Button al centre
        jbJugar.setAlignmentX(Component.CENTER_ALIGNMENT);
        //S'afegeix el Button de Jugar al Panell de Jugar i al Panell de Buttons
        jpJugar.add(jbJugar);
        jpJugar.setBorder(espai);
        jpButtons.add(jpJugar);

        //Es posa el Button al centre
        jbEspectador.setAlignmentX(Component.CENTER_ALIGNMENT);
        //S'afegeix el Button de Mode Espectador al Panell de Mode Espectador i al Panell de Buttons
        jpEspectador.add(jbEspectador);
        jpEspectador.setBorder(espai);
        jpButtons.add(jpEspectador);

        //Es posa el Button al centre
        jbReproduir.setAlignmentX(Component.CENTER_ALIGNMENT);
        //S'afegeix el Button de Reproduir Partida Antiga al Panell de Reproduir Partida Antiga i al Panell de Buttons
        jpReproduir.add(jbReproduir);
        jpReproduir.setBorder(espai);
        jpButtons.add(jpReproduir);

        //Es posa el Button al centre
        jbConfiguracio.setAlignmentX(Component.CENTER_ALIGNMENT);
        //S'afegeix el Button de Configuracio al Panell de Configuracio i al Panell de Buttons
        jpConfiguracio.add(jbConfiguracio);
        jpConfiguracio.setBorder(espai);
        jpButtons.add(jpConfiguracio);

        //Es posa el Button al centre
        jbSortir.setAlignmentX(Component.CENTER_ALIGNMENT);
        //S'afegeix el Button de Sortir al Panell de Sortir i al Panell de Buttons
        jpSortir.add(jbSortir);
        jpSortir.setBorder(espai);
        jpButtons.add(jpSortir);

        //S'afegeix el Panell de Buttons al Panell Principal del joc
        content.add(jpButtons);
        getContentPane().add(jpMenuPrincipal, BorderLayout.CENTER);
    }

    /**
     * Mètode que enllaça els botons amb el controlador de manera que aquest sàpiga què ha de gestionar.
     * @param listener ActionListener que ha de gestionar els esdeveniments.
     * @param viewController
     */
    public void registerController(ActionListener listener, ViewWindowController viewController) {
        //Registrem els ActionCommands als botons
        jbJugar.setActionCommand(JUGAR);
        jbEspectador.setActionCommand(MODE_ESPECTADOR);
        jbReproduir.setActionCommand(REPRODUIR);
        jbConfiguracio.setActionCommand(CONFIGURACIO);
        jbSortir.setActionCommand(SORTIR);

        //Fem que l'ActionListener respongui als esdeveniments dels botons
        jbJugar.addActionListener(listener);
        jbEspectador.addActionListener(listener);
        jbReproduir.addActionListener(listener);
        jbConfiguracio.addActionListener(listener);
        jbSortir.addActionListener(listener);

        addWindowListener(viewController);
    }
}
