package View;


import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class VistaPartidaAntiga extends JFrame {
    public static final String TORNA_AL_MENU = "Torna al menu ";

    private int numPartidesGuardades = 8;
    private JButton jbTornaMenu;

    public VistaPartidaAntiga() {
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());

        JPanel jpPanellPrincipal = new JPanel();
        jpPanellPrincipal.setLayout(new BoxLayout(jpPanellPrincipal, BoxLayout.Y_AXIS));

        EmptyBorder espai = new EmptyBorder(120,0,0,0);
        EmptyBorder espaiBottom = new EmptyBorder(0,0,50,0);

        jpPanellPrincipal.setBorder(espai);

        JPanel jpTaula = new JPanel();
        jpTaula.setLayout(new FlowLayout());

        JScrollPane jspTaula = new JScrollPane(jpTaula);
        jspTaula.setViewportBorder(null);

        JPanel jpFila = new JPanel(new GridLayout(numPartidesGuardades + 1,6));

        JLabel jlUsers = new JLabel("Users");
        Font fUsers = jlUsers.getFont();
        jlUsers.setFont(fUsers.deriveFont(fUsers.getStyle() | Font.BOLD));
        jlUsers.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlUsers);

        JLabel jlDate = new JLabel("Date");
        Font fDate = jlDate.getFont();
        jlDate.setFont(fDate.deriveFont(fDate.getStyle() | Font.BOLD));
        jlDate.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlDate);

        JLabel jlViewers = new JLabel("Viewers");
        Font fViewers = jlViewers.getFont();
        jlViewers.setFont(fViewers.deriveFont(fViewers.getStyle() | Font.BOLD));
        jlViewers.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlViewers);

        JLabel jlTime = new JLabel("Time");
        Font fTime = jlTime.getFont();
        jlTime.setFont(fTime.deriveFont(fTime.getStyle() | Font.BOLD));
        jlTime.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlTime);

        JLabel jlScore = new JLabel("Score");
        Font fScore = jlScore.getFont();
        jlScore.setFont(fScore.deriveFont(fScore.getStyle() | Font.BOLD));
        jlScore.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlScore);

        JLabel jlEntra = new JLabel("Entra");
        Font fEntra = jlEntra.getFont();
        jlEntra.setFont(fEntra.deriveFont(fEntra.getStyle() | Font.BOLD));
        jlEntra.setHorizontalAlignment(JLabel.CENTER);
        jpFila.add(jlEntra);


        for (int i = 1; i < (numPartidesGuardades * 6) + 1; i++) {
            if (i % 6 != 0) {
                JLabel jlEntra2 = new JLabel("Exemple");
                jlEntra2.setHorizontalAlignment(JLabel.CENTER);
                jpFila.add(jlEntra2);
            } else {
                JButton jbButton2 = new JButton("Entra");
                jbButton2.setHorizontalAlignment(JLabel.CENTER);
                jpFila.add(jbButton2);
            }
        }

        JPanel jpTornarMenu = new JPanel();
        jbTornaMenu = new JButton("Torna al menu");
        jpTornarMenu.setBorder(espaiBottom);
        jpTornarMenu.add(jbTornaMenu);


        jpTaula.add(jpFila);

        jpPanellPrincipal.add(jspTaula);


        getContentPane().add(jpPanellPrincipal, BorderLayout.CENTER);

        getContentPane().add(jpTornarMenu, BorderLayout.SOUTH);
    }

    public void registerController(ActionListener listener, ViewWindowController viewController) {
        jbTornaMenu.setActionCommand(TORNA_AL_MENU);
        jbTornaMenu.addActionListener(listener);

        addWindowListener(viewController);
    }
}

