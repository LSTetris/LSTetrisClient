package View;

import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

public class VistaConfiguracio extends JFrame {
    //Actions commands accessibles des de qualsevol classe
    public static final String TORNAR = "Tornar";
    public static final String GUARDAR = "Guardar";

    //Atributs que es necessiten per a implementar funcions
    private JButton jbTorna;
    private JButton jbGuardar;
    private JComboBox<String> jcbTeclaMoveLeft;
    private JComboBox<String> jcbTeclaMoveRight;
    private JComboBox<String> jcbTeclaMoveDown;
    private JComboBox<String> jcbTeclaTurnLeft;
    private JComboBox<String> jcbTeclaTurnRight;
    private JComboBox<String> jcbTeclaPause;

    /**
     * */
    public VistaConfiguracio() {
        //Propietats de la finestra
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());

        //Declaració del panell principal de la finestra
        JPanel jpConfiguracio = new JPanel();
        jpConfiguracio.setLayout(new GridBagLayout());
        Box content = Box.createVerticalBox();
        jpConfiguracio.add(content);

        //Declaració de l'espai
        EmptyBorder espai = new EmptyBorder(20,0,10,0);

        //Array de Strings amb les opcions de tecles
        String[] tecles = new String[] {"Selecciona la lletra...",
                "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
                "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "Left", "Right",
                "Up", "Down"
        };

        //Inicialització dels JComboBox amb l'array de les tecles
        jcbTeclaMoveLeft = new JComboBox<>(tecles);
        jcbTeclaMoveRight = new JComboBox<>(tecles);
        jcbTeclaMoveDown = new JComboBox<>(tecles);
        jcbTeclaTurnLeft = new JComboBox<>(tecles);
        jcbTeclaTurnRight = new JComboBox<>(tecles);
        jcbTeclaPause = new JComboBox<>(tecles);

        //Declaracio del panell de Moure a l'Esquerra. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpMoureEsquerra = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Moure a l'Esquerra
        JLabel jlMoureEsquerra = new JLabel("Tecla per moure a l'esquerra: ", SwingConstants.RIGHT);
        //S'afegeixen els components al panell de Moure a l'esquerra i al panell de Configuracio
        jpMoureEsquerra.add(jlMoureEsquerra);
        jpMoureEsquerra.add(jcbTeclaMoveLeft);
        jpMoureEsquerra.setBorder(espai);
        content.add(jpMoureEsquerra);

        //Declaracio del panell de Moure a la Dreta. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpMoureDreta = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Moure a la Dreta
        JLabel jlMoureDreta = new JLabel("Tecla per moure a la dreta: ", SwingConstants.RIGHT);
        //S'afegeixen els componens al panell de Moure a la dreta i al panell de Configuracio
        jpMoureDreta.add(jlMoureDreta);
        jpMoureDreta.add(jcbTeclaMoveRight);
        jpMoureDreta.setBorder(espai);
        content.add(jpMoureDreta);

        //Declaracio del panell de Moure a Baix. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpMoureBaix = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Moure a Baix
        JLabel jlMoureBaix = new JLabel("Tecla per moure a baix: ", SwingConstants.RIGHT);
        //S'afegeixen els components al panell de Moure a baix i al panell de Configuracio
        jpMoureBaix.add(jlMoureBaix);
        jpMoureBaix.add(jcbTeclaMoveDown);
        jpMoureBaix.setBorder(espai);
        content.add(jpMoureBaix);

        //Declaracio del panell de Girar a l'Esquerra. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpGirarEsquerra = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Girar a l'Esquerra
        JLabel jlGirarEsquerra = new JLabel("Tecla per girar a l'esquerra: ", SwingConstants.RIGHT);
        //S'afegeixen els components al panell de Girar a l'Esquerra i al panell de Configuracio
        jpGirarEsquerra.add(jlGirarEsquerra);
        jpGirarEsquerra.add(jcbTeclaTurnLeft);
        jpGirarEsquerra.setBorder(espai);
        content.add(jpGirarEsquerra);

        //Declaracio del panell de Girar a la Dreta. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpGirarDreta = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Girar a la Dreta
        JLabel jlGirarDreta = new JLabel("Tecla per girar a la dreta: ", SwingConstants.RIGHT);
        //S'afegeixen els components al panell de Girar a la Dreta i al panell de Configuracio
        jpGirarDreta.add(jlGirarDreta);
        jpGirarDreta.add(jcbTeclaTurnRight);
        jpGirarDreta.setBorder(espai);
        content.add(jpGirarDreta);

        //Declaracio del panell de Pausar. Una columna pel JLabel i l'altra pel JComboBox
        JPanel jpPausar = new JPanel(new GridLayout(1,2));
        //Declaracio del JLabel de Pausar
        JLabel jlPausar = new JLabel("Tecla per pausar el joc: ", SwingConstants.RIGHT);
        //S'afegeixen els components al panell de Pausar i al panell de Configuracio
        jpPausar.add(jlPausar);
        jpPausar.add(jcbTeclaPause);
        jpPausar.setBorder(espai);
        content.add(jpPausar);

        //Declaracio del panell dels Buttons
        JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jbTorna = new JButton("Tornar");
        jbGuardar = new JButton("Guardar");
        //S'afegeixen els buttons al panell de Buttons i al panell de Configuracio
        jpButtons.add(jbTorna);
        jpButtons.add(jbGuardar);
        jpButtons.setBorder(espai);
        content.add(jpButtons);

        //S'afegeix el panell de Conguracio al contenidor principal
        getContentPane().add(jpConfiguracio, BorderLayout.CENTER);
    }

    /**
     * Mètode que retorna els elements del combo box de configuració de tecles
     * @return ArrayList d'Strings amb els elements corresponents
     */
    public ArrayList<String> getSelectedKeys(){

        ArrayList<String> tecles = new ArrayList<>();
        tecles.add((String) jcbTeclaMoveLeft.getSelectedItem());
        tecles.add((String) jcbTeclaMoveRight.getSelectedItem());
        tecles.add((String) jcbTeclaMoveDown.getSelectedItem());
        tecles.add((String) jcbTeclaTurnLeft.getSelectedItem());
        tecles.add((String) jcbTeclaTurnRight.getSelectedItem());
        tecles.add((String) jcbTeclaPause.getSelectedItem());

        return tecles;

    }


    /**
     * Mètode que enllaça els botons amb el controlador de manera que aquest sàpiga què ha de gestionar.
     * @param listener ActionListener que ha de gestionar els esdeveniments.
     * @param viewController
     */
    public void registerController(ActionListener listener, ViewWindowController viewController) {
        //Registrem els ActionCommands als botons
        jbTorna.setActionCommand(TORNAR);
        jbGuardar.setActionCommand(GUARDAR);

        //Fem que l'ActionListener respongui als esdeveniments dels botons
        jbTorna.addActionListener(listener);
        jbGuardar.addActionListener(listener);

        addWindowListener(viewController);
    }
}
