package View;

import Controller.ViewWindowController;
import Controller.WindowController;
import Model.Match;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.LinkedList;

public class VistaEspectador extends JFrame {
    //Actions commands accessibles des de qualsevol classe
    public static final String TORNA_MENU = "Torna al menu";
    public static final String ENTRA = "Entra";

    private ButtonEditor buttonEditor;
    private ArrayList<ArrayList<String>> matches;
    private JButton jbTorna;
    private JTable jtMatches;

    public VistaEspectador(){
        matches = new ArrayList<ArrayList<String>>();

        DefaultTableModel dm = new DefaultTableModel();

        Object data [][] = generateDataVector();

        dm.setDataVector(data, new Object[]{"Nickname", "Temps", "Espectadors","Button"});

        buttonEditor = new ButtonEditor(new JCheckBox());

        jtMatches = new JTable(dm);
        jtMatches.getColumn("Button").setCellRenderer(new ButtonRenderer());
        jtMatches.getColumn("Button").setCellEditor(buttonEditor);
        JScrollPane scroll = new JScrollPane(jtMatches);

        jbTorna = new JButton("Torna enrrere");

        getContentPane().add( scroll );
        getContentPane().add(jbTorna, BorderLayout.SOUTH);

        //Propietats de la finestra
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);
    }

    public void setMatches(ArrayList<ArrayList<String>> matches) {
        this.matches = matches;

        DefaultTableModel dm = new DefaultTableModel();

        Object data [][] = generateDataVector();

        dm.setDataVector(data, new Object[]{"Nickname", "Temps", "Espectadors","Button"});


        buttonEditor = new ButtonEditor(new JCheckBox());

        jtMatches.setModel(dm);

        jtMatches.getColumn("Button").setCellRenderer(new ButtonRenderer());
        jtMatches.getColumn("Button").setCellEditor(buttonEditor);

        dm.fireTableDataChanged();
        jtMatches.revalidate();
        jtMatches.repaint();
    }

    public Object[][] generateDataVector () {
        Object result[][] = new Object[matches.size()][4];
        for (int i = 0; i < matches.size(); i++) {
            result[i][0] = matches.get(i).get(0);
            result[i][1] = matches.get(i).get(1);
            result[i][2] = matches.get(i).get(2);
            result[i][3] = "Entra";
        }
        return result;
    }

    public void registerController(WindowController controller, ViewWindowController viewWindowController) {
        buttonEditor.addButtonListener(controller);
        jbTorna.addActionListener(controller);
        jbTorna.setActionCommand(TORNA_MENU);

        addWindowListener(viewWindowController);
    }
}
/*
public class VistaEspectador extends JFrame {


    //Atributs que es necessiten per a implementar funcions
    private Thread proceso;
    private int numEspectadors = 4;
    private JButton jbTornaMenu;
    private ArrayList<JButton> jbEntra;
    private ArrayList<JLabel> jlPartides;
    private ArrayList<JLabel> jlEspectadors;
    private ArrayList<JLabel> jlTemps;
    private int quinBoto = 0;
    private JButton jbDelete;
    private JPanel jpEspectators;
    private ActionListener actionListener;
    private DefaultTableModel tableModel;
    private ArrayList<String> dades;
    private JTable jtbPartides;

    /**
     * Crea una nova VistaEspectador
     * @param matches son les partides que s'estan jugant actualment

    public VistaEspectador(ArrayList<Match> matches) {
        //Propietats de la finestra
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());


        //Fem le taula
        tableModel = new DefaultTableModel(matches.size(), 5);
        tableModel.setColumnIdentifiers(new String[]{"Nom", "Temps", "Espectadors", "Entrar", "Test"});

        jtbPartides = new JTable(tableModel);
        for (int i = 0; i < matches.size(); i++) {
            jtbPartides.add(new JLabel(matches.get(i).getNicknameJugador()));
            jtbPartides.add(new JLabel(Integer.toString(matches.get(i).getSecondsPlayed())));
            jtbPartides.add(new JLabel(Integer.toString(matches.get(i).getSpectators().size())));
            jtbPartides.add(new JButton("Entra"));
            jtbPartides.add(new JLabel("test"));
        }

        JScrollPane jScrollPanePartidas = new JScrollPane(jtbPartides);
        jScrollPanePartidas.setBorder(BorderFactory.createTitledBorder("Partides actives"));

        System.out.println(tableModel.getRowCount());

        this.setLayout(new BorderLayout());
        this.add(jScrollPanePartidas, BorderLayout.CENTER);


        jbEntra = new ArrayList<JButton>();

        //Declaracio del Panell del Button de Torna al Menu
        JPanel jpTornarMenu = new JPanel();
        //Declaracio del Button de Torna al Menu
        jbTornaMenu = new JButton("Torna al menu");

        //S'afegeix el Button al Panell de Torna al Menu
        jpTornarMenu.add(jbTornaMenu);

        //S'afegeix el Panell principal i el Panell de Torna Menu al contenidor principal
        getContentPane().add(jpTornarMenu, BorderLayout.SOUTH);

    }
*/