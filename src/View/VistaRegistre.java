package View;

import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class VistaRegistre extends JFrame {
    public static final String TORNA = "Torna";
    public static final String REGISTRAT = "Registra't";

    private JButton jbTorna;
    private JButton jbRegistrarse;

    private JTextField jtfNickname;
    private JTextField jtfCorreu;
    private JPasswordField jpfPassword;
    private JPasswordField jpfPassword2;

    public VistaRegistre() {
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());

        JPanel jpLogin = new JPanel();
        jpLogin.setLayout(new GridBagLayout());
        Box content = Box.createVerticalBox();
        jpLogin.add(content);



        EmptyBorder espai = new EmptyBorder(20,0,10,0);


        JPanel jpNickname = new JPanel(new BorderLayout(20,0));
        JLabel jlNickname = new JLabel("Nickname");
        jtfNickname = new JTextField(25);
        jpNickname.add(jlNickname, BorderLayout.LINE_START);
        jpNickname.add(jtfNickname, BorderLayout.CENTER);
        jpNickname.setBorder(espai);
        content.add(jpNickname);


        JPanel jpCorreu = new JPanel(new BorderLayout(20,0));
        JLabel jlCorreu = new JLabel("Correu electrònic");
        jtfCorreu = new JTextField(25);
        jpCorreu.add(jlCorreu, BorderLayout.LINE_START);
        jpCorreu.add(jtfCorreu, BorderLayout.CENTER);
        jpCorreu.setBorder(espai);
        content.add(jpCorreu);


        JPanel jpPassword = new JPanel(new BorderLayout(20,0));
        JLabel jlPassword = new JLabel("Contrasenya");
        jpfPassword = new JPasswordField(25);
        jpPassword.add(jlPassword, BorderLayout.LINE_START);
        jpPassword.add(jpfPassword, BorderLayout.CENTER);
        jpPassword.setBorder(espai);
        content.add(jpPassword);


        JPanel jpPassword2 = new JPanel(new BorderLayout(20,0));
        JLabel jlPassword2 = new JLabel("Repeteix la contrasenya");
        jpfPassword2 = new JPasswordField(25);
        jpPassword2.add(jlPassword2, BorderLayout.LINE_START);
        jpPassword2.add(jpfPassword2, BorderLayout.CENTER);
        jpPassword2.setBorder(espai);
        content.add(jpPassword2);


        JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        jbTorna = new JButton("Torna");
        jbRegistrarse = new JButton("Registra't");
        jpButtons.add(jbTorna);
        jpButtons.add(jbRegistrarse);
        jpButtons.setBorder(espai);
        content.add(jpButtons);


        getContentPane().add(jpLogin, BorderLayout.CENTER);
    }

    public void registerController(ActionListener listener, ViewWindowController viewController) {
        jbTorna.setActionCommand(TORNA);
        jbTorna.addActionListener(listener);

        jbRegistrarse.setActionCommand(REGISTRAT);
        jbRegistrarse.addActionListener(listener);

        addWindowListener(viewController);
    }

    public String[] getFields(){
        String[] fields = new String[4];

        fields[0] = this.jtfNickname.getText();
        fields[1] = this.jtfCorreu.getText();
        fields[2] = String.valueOf(this.jpfPassword.getPassword());
        fields[3] = String.valueOf(this.jpfPassword2.getPassword());

        return fields;
    }

}
