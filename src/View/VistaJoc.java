package View;

import Controller.KeyController;
import Controller.ViewWindowController;
import Controller.WindowController;
import Model.Match;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.concurrent.TimeUnit;
import java.awt.event.ActionListener;

public class VistaJoc extends JFrame {
    public static final String SORTIR_JOC = "Sortir ";
    public static final String GUARDAR_JOC = "Guardar ";

    private JPanel jpLateral;
    private JPanel jpTemps;
    private JLabel jlCounter;
    private Match match;
    private JPanelGame jPanelGame;
    //private JPanelFigure jPanelFigure;
    private JButton jbSortir;
    private JButton jbGuarda;

    public VistaJoc() {
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        this.setLayout(new BorderLayout());

        jPanelGame = new JPanelGame();
        //jPanelFigure = new JPanelFigure();

        jpLateral = new JPanel();
        jpLateral.setLayout(new GridBagLayout());
        Box content = Box.createVerticalBox();
        jpLateral.add(content);

        EmptyBorder espaiPrincipi = new EmptyBorder(50,0,50,0);
        EmptyBorder espai = new EmptyBorder(20,0,50,0);
        EmptyBorder espaiDoble = new EmptyBorder(150,0,10,0);


        jpTemps = new JPanel();
        jpTemps.setLayout(new BoxLayout(jpTemps, BoxLayout.Y_AXIS));
        JLabel jlTemps = new JLabel("Temps");
        jlTemps.setAlignmentX(Component.CENTER_ALIGNMENT);
        jlCounter = new JLabel("00:00");
        jlCounter.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpTemps.add(jlTemps);
        jpTemps.add(Box.createVerticalStrut(15));
        jpTemps.add(jlCounter);
        jpTemps.setBorder(espaiPrincipi);

        JPanel jpPuntuacio = new JPanel();
        jpPuntuacio.setLayout(new BoxLayout(jpPuntuacio, BoxLayout.Y_AXIS));
        JLabel jlPuntuacio = new JLabel("Puntuació");
        jlPuntuacio.setAlignmentX(Component.CENTER_ALIGNMENT);
        JLabel jlPunts = new JLabel("0");
        jlPunts.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpPuntuacio.add(jlPuntuacio);
        jpPuntuacio.add(Box.createVerticalStrut(15));
        jpPuntuacio.add(jlPunts);
        jpPuntuacio.setBorder(espai);

        JPanel jpEspectadors = new JPanel();
        jpEspectadors.setLayout(new BoxLayout(jpEspectadors, BoxLayout.Y_AXIS));
        JLabel jlEspectadors = new JLabel("Espectadors");
        jlEspectadors.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpEspectadors.add(jlEspectadors);
        jpEspectadors.setBorder(espai);

        JPanel jpSortir = new JPanel();
        jpSortir.setLayout(new BoxLayout(jpSortir, BoxLayout.Y_AXIS));
        jbSortir = new JButton("Sortir");
        jbSortir.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpSortir.add(jbSortir);
        jpSortir.setBorder(espaiDoble);

        JPanel jpGuarda = new JPanel();
        jpGuarda.setLayout(new BoxLayout(jpGuarda, BoxLayout.Y_AXIS));
        jbGuarda = new JButton("Guardar");
        jbGuarda.setAlignmentX(Component.CENTER_ALIGNMENT);
        jpGuarda.add(jbGuarda);


        content.add(jpTemps);
        content.add(jpPuntuacio);
        content.add(jpEspectadors);
        content.add(jpSortir);
        content.add(jpGuarda);


        add(jPanelGame, BorderLayout.CENTER);
        add(jpLateral, BorderLayout.EAST);
    }

    public void registerController(WindowController controller, KeyController keyController, ViewWindowController viewController/*, ActionListener listener*/) {
        jbSortir.setActionCommand(SORTIR_JOC);
        jbSortir.addActionListener(controller);

        jbGuarda.setActionCommand(GUARDAR_JOC);
        jbGuarda.addActionListener(controller);

        this.addKeyListener(keyController);
        this.setFocusable(true);

        addWindowListener(viewController);
    }

    private void pintaPartida2(char[][] prova){
        for(int y = 0; y < prova.length; y++){
            for(int x = 0; x < prova[y].length; x++){
                System.out.print(prova[y][x]);
            }
            System.out.println("");
        }
        System.out.println("-------");
    }

    public void registerMatch(Match match) {
        this.match = match;

        this.jPanelGame.registerMatch(match);
        this.jPanelGame.repaint();

        //this.jPanelFigure.registerNextFigure(match.getNextFigurePieces(), match.getNextFigureColor());
        //this.jPanelFigure.repaint();

        //pintaPartida2(match.getViewableBoard());
    }

    public void updateView() {
        this.jlCounter.setText(String.valueOf(TimeUnit.MILLISECONDS.toMinutes(match.getSecondsPlayed())) + ":" +
        String.valueOf(TimeUnit.MILLISECONDS.toSeconds(match.getSecondsPlayed()) -
                TimeUnit.MILLISECONDS.toMinutes(match.getSecondsPlayed()) * 60));
    }
}
