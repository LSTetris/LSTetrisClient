package View;

import Model.Match;

import javax.swing.*;
import java.awt.*;

public class JPanelGame extends JPanel{

    private Match match;

    public JPanelGame(){
    }

    public void registerMatch(Match match){
        this.match = match;
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        //Pintar Match

        //g.drawRect(120,80,270,594);
        //g.drawRect(10,140,250,550);
        //g.setColor(Color.LIGHT_GRAY);
        //g.fillRect(420,500,60,60);

        g.setColor(Color.BLACK);
        g.drawRect(120,60,270,594);

        //g.setColor(Color.CYAN);
        //g.fillRect(2 * 27, 2 * 27, 27, 27);

        if(match != null) {
            for (int y = 0; y < match.getBoard().length; y++) {
                for (int x = 0; x < match.getBoard()[0].length; x++) {
                    if (match.getViewableBoard()[y][x] != '0') {
                        //System.out.println("Pintar a X: " + x*27 + " Y: " + y*27 + " Char: " + match.getViewableBoard()[y][x]);
                        switch (match.getViewableBoard()[y][x]) {
                            case 'B':
                                //System.out.println("Pintar B");
                                g.setColor(Color.CYAN);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'J':
                                //System.out.println("Pintar J");
                                g.setColor(Color.BLUE);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'L':
                                //System.out.println("Pintar L");
                                g.setColor(Color.ORANGE);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'O':
                                //System.out.println("Pintar O");
                                g.setColor(Color.YELLOW);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'S':
                                //System.out.println("Pintar S");
                                g.setColor(Color.GREEN);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'T':
                                //System.out.println("Pintar T");
                                g.setColor(Color.MAGENTA);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                            case 'Z':
                                //System.out.println("Pintar Z");
                                g.setColor(Color.RED);
                                g.fillRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                                break;
                        }
                        g.setColor(Color.BLACK);
                        g.drawRect(x * 27 + 120,  y * 27 + 60, 27, 27);
                    }
                }
            }

            g.setColor(Color.WHITE);
            g.fillRect(4, 542, 112, 112);

            for (int y = 0; y < match.getNextFigurePieces().length; y++) {
                for (int x = 0; x < match.getNextFigurePieces()[0].length; x++) {
                    if(match.getNextFigurePieces()[y][x] != '0') {
                        switch (match.getNextFigureColor()) {
                            case 0:
                                g.setColor(Color.CYAN);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 1:
                                g.setColor(Color.BLUE);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 2:
                                g.setColor(Color.ORANGE);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 3:
                                g.setColor(Color.YELLOW);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 4:
                                g.setColor(Color.GREEN);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 5:
                                g.setColor(Color.MAGENTA);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                            case 6:
                                g.setColor(Color.RED);
                                g.fillRect(x * 27 + 6, y * 27 + 544, 27, 27);
                                break;
                        }

                        g.setColor(Color.BLACK);
                        g.drawRect(x * 27 + 6,  y * 27 + 544, 27, 27);
                    }
                }
            }
        }
    }

}
