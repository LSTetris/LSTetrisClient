package View;

import javax.swing.*;
import java.awt.*;

/**
 * La classe JPanelFigure permet l'implementacio d'un requadre on es visualitza una figura del LSTetris per informar al jugador de quina es la seguent figura.
 */
public class JPanelFigure extends JPanel {

    private char[][] nextFigurePieces;
    private int nextFigureColor;

    public JPanelFigure(){
        this.setSize(5000, 300);
    }

    public void registerNextFigure(char[][] nextFigurePieces, int nextFigureColor){
        this.nextFigurePieces = nextFigurePieces;
        this.nextFigureColor = nextFigureColor;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        if(nextFigurePieces != null){
            for (int y = 0; y < nextFigurePieces.length; y++) {
                for (int x = 0; x < nextFigurePieces[0].length; x++) {
                    switch(nextFigureColor) {
                        case 0:
                            g.setColor(Color.CYAN);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 1:
                            g.setColor(Color.BLUE);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 2:
                            g.setColor(Color.ORANGE);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 3:
                            g.setColor(Color.YELLOW);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 4:
                            g.setColor(Color.GREEN);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 5:
                            g.setColor(Color.MAGENTA);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                        case 6:
                            g.setColor(Color.RED);
                            g.fillRect(x * 27, y * 27, 27, 27);
                            break;
                    }
                }
            }
        }
    }
}
