package View;

import Controller.ViewWindowController;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

public class VistaIniciSessio extends JFrame {
    //Actions commands accessibles des de qualsevol classe
    public static final String INICIA_SESSIO = "Inicia Sessió";
    public static final String REGISTRARSE = "Registrar-se";

    //Atributs que es necessiten per a implementar funcions
    private JButton jbRegistrarse;
    private JButton jbIniciaSessio;
    private JTextField jtfNickname;
    private JPasswordField jpfPassword;

    /**
     * */
    public VistaIniciSessio() {
        //Propietats de la finestra
        setSize(500,700);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Tetrix");
        setLocationRelativeTo(null);

        getContentPane().setLayout(new BorderLayout());

        //Declaració del panell principal de la finestra
        JPanel jpLogin = new JPanel();
        jpLogin.setLayout(new GridBagLayout());
        Box content = Box.createVerticalBox();
        jpLogin.add(content);

        //Declaracio de l'espai
        EmptyBorder espai = new EmptyBorder(20,0,10,0);

        //Declaracio del panell del Nickname. Gap horitzontal de 20
        JPanel jpNickname = new JPanel(new BorderLayout(20,0));
        //Declaracio del JLabel del Nickname
        JLabel jlNickname = new JLabel("Nickname o correu");
        //Declaracio del JTextfield amb una llargada de 25
        jtfNickname = new JTextField(25);
        //S'afegeixen els components al panell de Nickname i al panell de login
        jpNickname.add(jlNickname, BorderLayout.LINE_START);
        jpNickname.add(jtfNickname, BorderLayout.CENTER);
        jpNickname.setBorder(espai);
        content.add(jpNickname);

        //Declaracio del panell de Password. Gap horitzontal de 20
        JPanel jpPassword = new JPanel(new BorderLayout(20,0));
        //Declaracio del JLabel de Password
        JLabel jlPassword = new JLabel("Contrasenya");
        //Declaracio de JPasswordField amb una llargada de 25
        jpfPassword = new JPasswordField(25);
        //S'afegeixen els components al panell de Password i al panell de login
        jpPassword.add(jlPassword, BorderLayout.LINE_START);
        jpPassword.add(jpfPassword, BorderLayout.CENTER);
        jpPassword.setBorder(espai);
        content.add(jpPassword);

        //Declaracio del panell de botons
        JPanel jpButtons = new JPanel(new FlowLayout(FlowLayout.CENTER));
        //Declaracio dels botos de Registrar-se i Inicia Sessio
        jbRegistrarse = new JButton("Registrar-se");
        jbIniciaSessio = new JButton("Inicia sessió");
        //S'afegeixen els botons al panell de Buttons i al panell de login.
        jpButtons.add(jbRegistrarse);
        jpButtons.add(jbIniciaSessio);
        jpButtons.setBorder(espai);
        content.add(jpButtons);

        //S'agefeix el panell de Login al contenidor principal
        getContentPane().add(jpLogin, BorderLayout.CENTER);
    }

    /**
     * Mètode que enllaça els botons amb el controlador de manera que aquest sàpiga què ha de gestionar.
     * @param listener ActionListener que ha de gestionar els esdeveniments.
     * @param viewController
     */
    public void registerController(ActionListener listener, ViewWindowController viewController) {
        //Registrem els ActionCommands als botons
        jbIniciaSessio.setActionCommand(INICIA_SESSIO);
        jbRegistrarse.setActionCommand(REGISTRARSE);

        //Fem que l'ActionListener respongui als esdeveniments dels botons
        jbIniciaSessio.addActionListener(listener);
        jbRegistrarse.addActionListener(listener);

        addWindowListener(viewController);
    }

    public void setJtfNickname(String jtfNickname) {
        this.jtfNickname.setText(jtfNickname);
    }

    public void setJpfPassword(String jpfPassword) {
        this.jpfPassword.setText(jpfPassword);
    }


    /**
     * */
    public String[] getFields(){
        String[] fields = new String[2];

        fields[0] = jtfNickname.getText();
        fields[1] = String.valueOf(jpfPassword.getPassword());

        return fields;
    }

}
