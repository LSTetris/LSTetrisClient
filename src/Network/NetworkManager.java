package Network;

import Controller.WindowController;
import Model.Match;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * La classe NetworkManager es el gestor de xarxa del client del joc LSTetris. Permet la comunicacio amb un servidor dedicat.
 */
public class NetworkManager extends Thread {

    //Atributs necessaris
    //private ViewController controller;
    private final ObjectOutputStream oos;
    private final ObjectInputStream ooi;
    private boolean running;
    private WindowController controller;

    //Id del jugador per poder fer gestions de la BBDD
    private String playerUsername;

    /**
     * Crea un nou NetworkManager.
     * @param IP es l'adreça IP del servidor dedicat al que es conectara
     * @param PORT es el port del servidor dedicat al que es conectara
     * @throws IOException es una excepcio que es pot llençar si no hi ha bona connectivitat client-servidor.
     */
    public NetworkManager (String IP, int PORT) throws IOException {
        Socket socket = new Socket(IP, PORT);
        oos = new ObjectOutputStream(socket.getOutputStream());
        ooi = new ObjectInputStream(socket.getInputStream());
    }

    /**
     * Habilita la connexio amb el servidor.
     */
    public void startServerConnection (){ //ViewController controller) {
        //this.controller = controller;
        running = true;
        start();
    }

    @Override
    public void run () {
        try {
            while (running) {
                //Esperem que entri qualsevol cosa pel socket
                //System.out.println("Waiting to read...");
                StatusMessage information = (StatusMessage)ooi.readObject();
                handleMessage(information);
            }
        } catch(SocketException e){
            System.out.println("Server was shutdown");
            running = false;
            System.exit(1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            //En cas d'error ho enviem pel controlador que ho gestiona
            //controller.manageNetworkError(e.getMessage());
            e.printStackTrace();
            //S'apaga el programa ja que no pot fer res sense connexió
            System.exit(1);
        }
    }

    /**
     * Atura la connexio amb el servidor
     */
    public void stopServerConnection () {
        sendObject(new String[]{"Disconnect"}, playerUsername);
        running = false;
        interrupt();
    }

    /**
     * Envia un objecte al servidor amb un seguit d'arguments informatius
     * @param arguments son arguments que informen sobre el contingut del missatge
     * @param o es l'objecte que s'envia i el contingut del missatge
     */
    public void sendObject(String[] arguments, Object o){
        try {
            oos.writeObject(new StatusMessage(arguments, o));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Registra un controlador al gestor de xarxa
     * @param windowController es el controlador
     */
    public void registerController(WindowController windowController) {
        this.controller = windowController;
    }

    /**
     * Analitza un missatge i executa les commandes convenients. S'utilitza per respondre a peticions del servidor.
     * @param information es el missatge rebut del servidor
     */
    public void handleMessage(StatusMessage information) {
        switch(information.getArguments()[0]){
            case "Information":
                switch(information.getArguments()[1]){
                    case "Screen":
                        System.out.println((String)information.getInformation());
                        break;
                }
                break;
            case "Game":
                switch(information.getArguments()[1]) {
                    case "Status":
                        //System.out.println("Rebut estatus del joc");
                        //System.out.println(((Match) information.getInformation()).getSecondsPlayed());
                        this.controller.registerGame((Match) information.getInformation());
                        this.controller.updateGameView();
                        break;
                    case "End":
                        this.controller.missatgeFiPartida();
                }
                break;
            case "Register":
                switch(information.getArguments()[1]){
                    case "Successful":
                        controller.switchToMenuFromRegister();
                        playerUsername = (String) information.getInformation();

                        break;
                    case "Error":
                        //TODO: Mostrar errors a la vista
                        System.out.println(information.getInformation());
                }
                break;
            case "Login":
                switch(information.getArguments()[1]){
                    case "Successful":
                        controller.switchToMenuFromLogin();
                        playerUsername = (String) information.getInformation();

                        break;
                    case "Error":
                        controller.missatgeLoginIncorrecte();
                        break;
                }
                break;
            case "Spectator":
                switch(information.getArguments()[1]){
                    case "Players":
                        ArrayList<ArrayList<String>> games = (ArrayList<ArrayList<String>>) information.getInformation();

                        System.out.println("---------------");
                        for (int i = 0; i < games.size(); i++) {
                            System.out.println(games.get(i));
                        }
                        System.out.println("---------------");

                        controller.actualitzaVistaEspectador(games);

                        break;
                    case "Play":

                        break;
                }
        }
    }

    /*public String getPlayerID() {
        return playerID;
    }*/
}
