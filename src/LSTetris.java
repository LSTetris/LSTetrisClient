import Controller.KeyController;
import Controller.ViewWindowController;
import Controller.WindowController;
import Model.ConfigParser;
import Model.Match;
import Model.UserManager;
import Network.NetworkManager;
import View.*;

import javax.swing.*;
import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;

public class LSTetris {

    public static void main(String[] args) {


        ConfigParser config = new ConfigParser();

        /* S'invoca Swing*/
        SwingUtilities.invokeLater(() -> {
            // Crear les vistes
            VistaMenu vistaMenu = new VistaMenu();
            VistaJoc vistaJoc = new VistaJoc();
            VistaIniciSessio vistaIniciSessio = new VistaIniciSessio();
            VistaRegistre vistaRegistre = new VistaRegistre();
            VistaConfiguracio vistaConfiguracio = new VistaConfiguracio();

            VistaEspectador vistaEspectador = new VistaEspectador();
            VistaPartidaAntiga vistaAntiga = new VistaPartidaAntiga();


            vistaIniciSessio.setVisible(true);

            try {
                //Crear model
                UserManager manager = new UserManager();

                // Crear el Network
                NetworkManager networkManager = new NetworkManager(config.getServerIp(), config.getClientPort());

                // Crear els controladors i enllaçar C->V i C->M
                ViewWindowController viewController = new ViewWindowController(networkManager, vistaMenu);
                WindowController controller = new WindowController(vistaMenu, vistaJoc, vistaIniciSessio, vistaRegistre, vistaConfiguracio, vistaEspectador, vistaAntiga, manager, networkManager);
                KeyController keyController = new KeyController(networkManager);

                // Fer la relació V->C i S->C
                networkManager.registerController(controller);
                vistaMenu.registerController(controller, viewController);
                vistaIniciSessio.registerController(controller, viewController);
                vistaRegistre.registerController(controller, viewController);
                vistaConfiguracio.registerController(controller, viewController);
                vistaJoc.registerController(controller, keyController, viewController);
                vistaEspectador.registerController(controller, viewController);
                vistaAntiga.registerController(controller, viewController);

                networkManager.startServerConnection();

            } catch (ConnectException e) {
                //view.showError("Could not connect to server");
                System.out.println("Could not connect to server");
                //System.exit(0);
            } catch (IOException e) {
                e.printStackTrace();
            }


        });
    }
}
