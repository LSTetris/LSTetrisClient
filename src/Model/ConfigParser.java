package Model;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * La classe ConfigParser permet l'introduccio d'una configuracio a partir d'un fitxer extern 'config.json'.
 */
public class ConfigParser {
    private String serverIp;
    private int clientPort;

    /**
     * Crea un nou ConfigParser.
     */
    public ConfigParser() {
        FileReader f;
        JsonObject jsonObject = null;
        JsonParser parser = new JsonParser();

        try {
            f = new FileReader(new File("config.json"));
            jsonObject = (JsonObject) parser.parse(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        this.serverIp = jsonObject.get("server_ip").getAsString();
        this.clientPort = jsonObject.get("server_port").getAsInt();
    }

    /**
     * Retorna la IP del servidor al qual es conectara el client
     * @return la IP del servidor
     */
    public String getServerIp() {
        return serverIp;
    }

    /**
     * Retorna el port del servidor al qual es conectara el client
     * @return es el port del servidor
     */
    public int getClientPort() {
        return clientPort;
    }
}
