package Model;

import java.io.Serializable;

/**
 * La classe user conté l'informacio referent als usuaris del joc
 */
public class User implements Serializable{

    private String nickname;
    private String email;
    private String password;


    /**
     * Crea un nou user a partir d'un array de String
     * @param data els Strings que contenen, en ordre, el nickname, l'email i la contrasenya de l'usuari
     */
    public User(String[] data){
        this.nickname = data[0];
        this.email = data[1];
        this.password = data[2];

    }

    /**
     * Retorna el nickname de l'usuari
     * @return el nickname de l'usuari
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Retorna el mail de l'usuari
     * @return el mail de l'usuari
     */
    public String getEmail() {
        return email;
    }

    /**
     * Retorna la contrasenya de l'usuari
     * @return la contrasenya de l'usuari
     */
    public String getPassword() {
        return password;
    }
}
