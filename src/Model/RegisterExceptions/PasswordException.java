package Model.RegisterExceptions;

/**
 * La classe PasswordException es l'excepcio que es llença quan una contrasenya no compleix amb el format valid
 */
public class PasswordException extends RegisterException {
    /**
     * Crea una nova PasswordException amb un missatge d'error
     * @param s es el missatge d'error
     */
    public PasswordException(String s) {
        super(s);
    }
}
