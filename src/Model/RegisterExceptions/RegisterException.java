package Model.RegisterExceptions;

/**
 * La classe RegisterException es l'excepcio que es llença quan el registre a LSTetris no ha sigut valid.
 */
public class RegisterException extends Exception {
    /**
     * Crea una nova RegisterException amb un missatge d'error
     * @param s es el missatge d'error
     */
    public RegisterException(String s) {
        super(s);
    }
}
