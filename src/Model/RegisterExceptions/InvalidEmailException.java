package Model.RegisterExceptions;

/**
 * La classe InvalidEmailException es l'exception que es llença quan un email no presenta una estructura valida
 */
public class InvalidEmailException extends RegisterException {
    /**
     * Crea un nou InvalidEmailException amb un missatge d'error
     * @param s es el missatge d'error
     */
    public InvalidEmailException(String s) {
        super(s);
    }
}
