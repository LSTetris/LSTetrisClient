package Model.RegisterExceptions;

/**
 * La classe EmptyFieldException es l'exception que es llença quan un dels camps del registre es buit
 */
public class EmptyFieldException extends RegisterException {
    /**
     * Crea una nova EmptyFieldException amb un missatge
     * @param s es el missatge de l'excepcio
     */
    public EmptyFieldException(String s) {
        super(s);
    }
}
